﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Maze : MonoBehaviour {

	public GameObject wall;
	public float wallLength;
	public int xSize;
	public int ySize;
	public Cell[] cells;
	public int currentCell = 0;

	int totalCells;
	int visitedCells = 0;
	bool staredBuilding = false;
	int currentNeighbour = 0;
	Vector3 initialPos;
	GameObject wallHolder;
	List<int> lastCells;
	int backingUp = 0;
	int wallToBreak = 0;

	void Start () {
		CreateWalls ();
	}

	void CreateWalls () {
		wallHolder = new GameObject ();
		wallHolder.name = "Maze";

		initialPos = new Vector3 ((-xSize / 2) + wallLength / 2, 0, (-ySize / 2) + wallLength / 2);
//		print ("init: " + initialPos);
		Vector3 myPos = initialPos;
		GameObject tempWall;

		for (int i = 0; i < ySize; i++) {
			for (int j = 0; j <= xSize; j++) {
				myPos = new Vector3 (initialPos.x + (j * wallLength) - wallLength / 2, 0, initialPos.z + (i * wallLength) - wallLength / 2);
//				print (myPos);
				tempWall = Instantiate (wall, myPos, Quaternion.identity) as GameObject;
				tempWall.SetActive (false);
				tempWall.transform.parent = wallHolder.transform;
			}
		}
		//for y axis
		for (int i = 0; i <= ySize; i++) {
			for (int j = 0; j < xSize; j++) {
				myPos = new Vector3 (initialPos.x + (j * wallLength), 0, initialPos.z + (i * wallLength) - wallLength);
//				print (myPos);
				tempWall = Instantiate (wall, myPos, Quaternion.Euler(0, 90, 0) ) as GameObject;
				tempWall.SetActive (false);
				tempWall.transform.parent = wallHolder.transform;
			}
		}

		CreateCells ();

	}

	void CreateCells () {
		lastCells = new List<int> ();
		lastCells.Clear ();
		totalCells = xSize * ySize;
		int children = wallHolder.transform.childCount;
//		print ("children" + children);
		GameObject[] allWalls = new GameObject[children];
		cells = new Cell[xSize * ySize];
		int eastWeastProcess = 0;
		int childProcess = 0;
		int termCount = 0;

		//Get All the children 
		for (int i = 0; i < children; i ++) {
			allWalls [i] = wallHolder.transform.GetChild (i).gameObject;
		}

		for (int cellprocess = 0; cellprocess < cells.Length; cellprocess++) {
			cells [cellprocess] = new Cell ();
			cells [cellprocess].east = allWalls [eastWeastProcess];
			cells [cellprocess].south = allWalls [childProcess + (xSize+1) * ySize];
			if (termCount == xSize) {
				eastWeastProcess += 2;
				termCount = 0;
			} else {
				eastWeastProcess++;
			}
			termCount++;
			childProcess++;

			cells [cellprocess].west = allWalls [eastWeastProcess];
			cells [cellprocess].north = allWalls [ (childProcess + (xSize+1) * ySize) + xSize -1];

		}

		CreateMaze ();
	}

	void CreateMaze () {
		while (visitedCells < totalCells) {
			if (staredBuilding) {
				GiveMeNeighbour ();
				if (cells [currentNeighbour].visited == false && cells [currentCell].visited == true) {
					BreakWall ();
					cells [currentNeighbour].visited = true;
					visitedCells++;
					lastCells.Add (currentCell);
					currentCell = currentNeighbour;
					if (lastCells.Count > 0) {
						backingUp = lastCells.Count - 1;
					}
				}
			} 
			else {
				currentCell = Random.Range (0, totalCells);
				cells [currentCell].visited = true;
				visitedCells++;
				staredBuilding = true;
			}

		}
		Debug.Log ("finished");

		GiveMeNeighbour ();

		Exits  ex = GetComponent<Exits> ();
		ex.Init ();
	}

	void BreakWall() {
		switch (wallToBreak) {
		case 1:
			Destroy (cells [currentCell].north);
			break;
		case 2:
			Destroy (cells [currentCell].east);
			break;
		case 3:
			Destroy (cells [currentCell].west);
			break;
		case 4:
			Destroy (cells [currentCell].south);
			break;

		}
	}

	void GiveMeNeighbour () {
		
		int length = 0;
		int[] neighgours = new int[4];
		int[] connectingWall = new int[4];
		int check = 0;
		check = ((currentCell + 1) / xSize);
		check -= 1;
		check *= xSize;
		check += xSize;
		//west 
		if (currentCell + 1 < totalCells &&  (currentCell +1) != check) {
			if (cells[currentCell + 1].visited == false) {
				neighgours [length] = currentCell + 1;
				connectingWall [length] = 3;
				length++;
			}
		}

		//east
		if (currentCell - 1 >= 0 && currentCell != check) {
			if (cells[currentCell - 1].visited == false) {
				neighgours [length] = currentCell - 1;
				connectingWall [length] = 2;
				length++;
			}
		}

		//north
		if (currentCell + xSize < totalCells) {
			if (cells[currentCell + xSize].visited == false) {
				neighgours [length] = currentCell +xSize;
				connectingWall [length] = 1;
				length++;
			}
		}

		//south
		if (currentCell - xSize >= 0) {
			if (cells[currentCell - xSize].visited == false) {
				neighgours [length] = currentCell - xSize;
				connectingWall [length] = 4;
				length++;
			}
		}

		if (length != 0) {
			int theChoosenOne = Random.Range (0, length);
			currentNeighbour = neighgours [theChoosenOne];
			wallToBreak = connectingWall [theChoosenOne];

		} 
		else {
			if (backingUp > 0) {
				currentCell = lastCells [backingUp];
				backingUp--;
			}
		}

//		for (int i = 0; i < length; i++) {
//			Debug.Log (neighgours[i]);	
//		}
//

	}



}
