﻿using System;
using UnityEngine;

[System.Serializable]
public class Cell {
	public bool visited;
	public GameObject north; //1
	public GameObject east; //2
	public GameObject west; //3
	public GameObject south;//4
}
