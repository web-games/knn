﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameRoot: MonoBehaviour{

	public int current = 25;
	public int points = 0;
	public Text textTime;
	public Text pointsText; 

	Exits exits;
	Coroutine countRotine;
	Coroutine updateRotine;

	void Awake () {
		exits = GetComponent<Exits> ();
	}

	void Start () {
		updateRotine = StartCoroutine (DoUpdate ());
		countRotine = StartCoroutine (Count ());
	}

	IEnumerator DoUpdate () {
		while (true) {
			if (current < 1) {
				Debug.Log ("GameOver");
				StopCoroutine (countRotine);
				exits.Stop ();
				break;
			}
			yield return new WaitForSeconds(.2f);
		}
	}

	public IEnumerator Count () {
		print ("TIMER");
		current = 26;
		while (true) {
			current--;
			textTime.text = "Time: " + current.ToString ();
			pointsText.text = "Points: " + points.ToString ();
			yield return new WaitForSeconds (1f);
		}
	}
}

