﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Exits : MonoBehaviour {

	public Transform exitPoint;
	public Transform playerPrefab;
	public Transform player;
	public Transform nearestExit;
	public Transform exit;

	public Transform[] exits = new Transform[5];
	public Vector3 nearestVec;
	public Vector3[] positions = new Vector3[5];
	string indexes = "";

	Maze maze;
	Coroutine restartRoutine;

	void Awake() {
		maze = GetComponent<Maze> ();
	}

	public void Init() {
		SortPositions ();
	}

	private IEnumerator RestartAnimation () {
		yield return new WaitForSeconds(5f);
		print("Restarting");

		DestroyExits ();
		SortPositions ();
	}

	public void Restart() {
		restartRoutine = StartCoroutine (RestartAnimation());
	}

	public void Stop() {
		DestroyExits ();
	}

	public void DestroyExits() {
		print ("indexes:" + indexes.ToString());
		print ("Destroiyng");
		indexes = "";

		print ("indexes:" + indexes);
		GameObject[] allExits = GameObject.FindGameObjectsWithTag ("exit");
		foreach (GameObject o in allExits) {
			GameObject.Destroy (o);
		}
		GameObject p = GameObject.FindGameObjectWithTag ("Player");
		GameObject.Destroy (p);
	} 


	public void SortPositions () {
		Maze maze = GetComponent<Maze> ();
		Cell[] cells = maze.cells;

		int index = 0;
		string indexString = "";

		for (int i = 0; i < 5; i++) {
			index = Random.Range (0, cells.Length -1);
			indexString = " _" + index.ToString () + "_ ";	

			while (indexes.Contains (indexString)) {
				index = Random.Range (0, cells.Length -1);
				indexString = index.ToString () + "_";
			}

			indexes += indexString;
			Cell currentCell = cells [index];
				
			print ("ERROR: " + i);

			float x = 0, z = 0;
			if (currentCell.west) {
				x = currentCell.west.transform.position.x - 2.5f;
				z = currentCell.west.transform.position.z;
			} else if (currentCell.east) { 
				x = currentCell.east.transform.position.x + 2.5f;
				z = currentCell.east.transform.position.z;
			} else if (currentCell.north) {
				x = currentCell.north.transform.position.x - 2.5f;
				z = currentCell.north.transform.position.z;
			} else {
				x = currentCell.south.transform.position.x + 2.5f;
				z = currentCell.south.transform.position.z;
			}


			Vector3 pos = new Vector3 (x, 0.5f, z);
			positions [i] = pos;
		}

		MakeExitPoints ();
		SpawnPlayer ();

	}

	void SpawnPlayer () {
		player = Instantiate (playerPrefab, positions[positions.Length -1], Quaternion.identity) as Transform;
		FindKNN ();
	}

	void MakeExitPoints () {
		for(int i = 0; i < 4; i++) {
			Transform exitP = Instantiate (exitPoint, positions[i], Quaternion.identity) as Transform;
			exits [i] = exitP;		
		}
	}

	void FindKNN() {
		Vector3 v = player.position;
		Debug.Log("Player pos: " + v);

		float minResult = -1f;
		int minIndex = -1;

		for(int i = 0; i < exits.Length; i ++) {	
			Vector3 w = exits [i].position;
			float a = Mathf.Pow((w.x - v.x), 2); 
			float b = Mathf.Pow(( w.y - v.y), 2);
			float c = Mathf.Pow((w.z - v.z), 2);
			float result = Mathf.Sqrt (a + b + c);
			if (result < minResult || minResult < 0) {
				minResult = result;
				minIndex = i;
			}
		}
		print ("nearest vec: " + exits[minIndex].position);
		Instantiate (nearestExit, exits [minIndex].position, Quaternion.identity);

		for (int i = 0; i < exits.Length; i++) {
			if (i != minIndex) {
				Instantiate (exit, exits [i].position, Quaternion.identity);
			}
		}
	}
		

}
