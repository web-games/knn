﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionCheck : MonoBehaviour {

	Exits exits;
	GameRoot root;

	public int value;

	Coroutine abductionRoutine;

	void Awake () {
		exits = GameObject.FindObjectOfType<Exits> ();
		root = GameObject.FindObjectOfType<GameRoot> ();
	} 

	void OnTriggerEnter (Collider other) {
		print (other.name);
		if (other.tag == "Player") {
			exits.Restart ();
			root.current = 26;
			root.points += 1 * value;
			abductionRoutine = StartCoroutine (Abduction (other.transform));

		}
	}

	IEnumerator Abduction (Transform t) {
		float y = 1;
		while (true) {
			t.position = new Vector3 (t.position.x, y, t.position.z);
			y++;
			yield return new WaitForEndOfFrame ();
		}
	}

}
